package org.lionzhou.chatGG.web.util;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description
 * @Author ZhouLiangCheng
 * @Date 2023/2/10
 * @Version 1.0
 */
@Data
public class TextDto implements Serializable {
    String content;
}

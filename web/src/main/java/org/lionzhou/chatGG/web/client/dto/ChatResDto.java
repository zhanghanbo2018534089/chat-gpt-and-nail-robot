package org.lionzhou.chatGG.web.client.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Author ZhouLiangCheng
 * @Date 2023/2/10
 * @Version 1.0
 */
@Data
public class ChatResDto implements Serializable {
    String code;
    String message;
    String requestid;

    String id;
    String object;
    Integer created;
    String model;
    List<ChatChoiceItemDto> choices;
    Map<String, Object> usage;

}

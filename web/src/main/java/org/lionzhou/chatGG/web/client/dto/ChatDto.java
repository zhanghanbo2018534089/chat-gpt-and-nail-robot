package org.lionzhou.chatGG.web.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @Description
 * @Author ZhouLiangCheng
 * @Date 2023/2/10
 * @Version 1.0
 */
@Data
public class ChatDto implements Serializable {

    String model = "text-davinci-003";
    String prompt;
    Integer temperature = 0;
    @JsonProperty("max_tokens")
    Integer maxTokens = 4000;

    public String toPost() {
        String body = String.format("model=%s&prompt=%s&temperature=%d&max_tokens=%d", encode(getModel()), encode(getPrompt()), getTemperature(), getMaxTokens());
        return body;
    }

    private String encode(String data) {
        return URLEncoder.encode(data, StandardCharsets.UTF_8);
    }

    private String encode(String key, List<Object> data) {
        if (data.size() < 1) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (Object d : data) {
            sb.append(String.format("%s=%s&", key, URLEncoder.encode(d.toString(), StandardCharsets.UTF_8)));
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }
}

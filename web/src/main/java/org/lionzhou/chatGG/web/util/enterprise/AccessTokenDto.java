package org.lionzhou.chatGG.web.util.enterprise;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description
 * @Author ZhouLiangCheng
 * @Date 2023/2/11
 * @Version 1.0
 */
@Data
public class AccessTokenDto implements Serializable {
    String accessToken;
    Long expireIn;
}
